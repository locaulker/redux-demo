import * as actions from "./actions"

// define initial state
const initData = {
  users: ["admin", "manager", "end-user"],
  loginDet: "none",
}

// define the reducer function
const loginReducer = (state = initData, action) => {
  if (action.type === actions.LOGIN) {
    return {
      ...state,
      loginDet: action.loginDet,
    }
  }

  return state
}

// exporting the store
export default loginReducer
